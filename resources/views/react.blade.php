
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Анонимные комментарии</title>
    <link rel="stylesheet" href="{{url('css/app.css')}}" />
</head>
<body>
@yield('content')
<script src="/js/react.js"></script>
<script src="/js/react-dom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.24/browser.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@yield('js')
</body>
</html>
