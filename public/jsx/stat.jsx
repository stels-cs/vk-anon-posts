
var aStyle = {
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0
};

var panelStyle = {
    position:'relative'
};

var StatBox = React.createClass({
    loadFromServer : function () {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({stat: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    getInitialState: function() {
        return {stat: {
            posts : 'wait...'
        }};
    },
    componentDidMount: function() {
        this.loadFromServer();
        setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    },
    render : function () {
        return (
            <div className="panel panel-info">
                <div className="panel-heading">
                    Stat
                </div>
                <div className="panel-body">
                    <div>Total reviews: {this.state.stat.posts}</div>
                </div>
            </div>
        );
    }
});

var StarBox = React.createClass({

    render: function () {
        return (
            <span>{this.props.star}</span>
        );
    }

    });

var Review = React.createClass({
    getInitialState: function() {
        return {className: 'panel-default'};
    },
    getLink : function () {
        return "https://vk.com/id" + this.props.data.user_id;
    },
    tick : function () {
        this.setState({className: 'panel-default'});
    },
    componentDidMount: function() {
        var now = new Date();
        var createdTime = new Date(this.props.data.created_at);
        if (now.getTime() - createdTime.getTime() < 60 * 1000) {
            this.setState({className: 'panel-success'});
            console.log("set");
        }
        this.interval = setTimeout(this.tick, 60000);
    },
    componentWillUnmount: function() {
        clearTimeout(this.interval);
    },
    render: function () {
        return (
            <div className="col-sm-4">
            <div className={"panel " + this.state.className}>
                <div className="panel-heading">#{this.props.data.id} Name: {this.props.data.name}</div>
                <div className="panel-body" style={panelStyle}>
                    <StarBox star={this.props.data.star}/>
                    <p>{this.props.data.text}</p>
                    <span>{this.props.data.created_at}</span>
                    <a style={aStyle} href={this.getLink()} target="_blank"></a>
                </div>
            </div>
            </div>
        );
    }
});

var ReviewList = React.createClass({
    render:function () {
        var commentNodes = this.props.data.map(function(comment) {
            return (
                <Review data={comment} key={comment.id}>
                </Review>
        );
        });
        return (
            <div>
            {commentNodes}
            </div>
        );
    }
});

var ReviewBox = React.createClass({
    loadCommentsFromServer: function() {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    getInitialState: function() {
        return {data: []};
    },
    componentDidMount: function() {
        this.loadCommentsFromServer();
        setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    },
    render: function () {
        return (
            <ReviewList  data={this.state.data} />
        );
    }
});

var MainLayout = React.createClass({
    render : function () {
        return (
            <div className="row">
                <div className="col-xs-12">
                    <h2>Anon reviews statistic</h2><hr/>
                </div>
                <div className="col-sm-3 visible-xs">
                    <StatBox url="/total" pollInterval="15000" />
                </div>
                <div className="col-sm-9">
                    <ReviewBox url="/last" pollInterval={5000} />
                </div>
                <div className="col-sm-3 hidden-xs">
                    <StatBox url="/total" pollInterval={5000}  />
                </div>
            </div>
        );
    }
});

ReactDOM.render(
    <MainLayout />,
    document.getElementById('example')
);