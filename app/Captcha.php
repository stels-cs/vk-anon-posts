<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 24.10.15
 * Time: 1:19
 */

namespace App;

use App;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Request;

class Captcha extends Model {


	protected $table = 'captcha';

	protected $fillable = [

	];

	protected $dates = [
		'created_at','updated_at'
	];

	protected $hidden = ['code','created_at','updated_at','deleted_at','id'];


	protected $casts = [
		'active' => 'boolean'
	];


	public function toArray() {
		return [
			'img' => $this->getFilePath(true),
			'key' => $this->getKey()
		];
	}

	public function getKey() {
		return $this->id.'i'.$this->key;
	}

	public static function boot() {
		parent::boot();

		self::creating(function ($model) {
			$model->key = uniqid(time(),true);
			$model->code = strtoupper( md5(rand(5,99).time().uniqid()) );
			$model->code = substr($model->code, rand(1,2), 5);
		});

		self::saved( function($model)  {
			$file = $model->getFilePath();
			$code = $model->code;

			$bg_image = $model->getBgPath();
			$bg_image_info = getimagesize($model->getBgPath());
			if ($bg_image_info['mime'] == 'image/jpg' || $bg_image_info['mime'] == 'image/jpeg') {
				$old_image = imagecreatefromjpeg($bg_image);
			} elseif ($bg_image_info['mime'] == 'image/gif') {
				$old_image = imagecreatefromgif($bg_image);
			} elseif ($bg_image_info['mime'] == 'image/png') {
				$old_image = imagecreatefrompng($bg_image);
			}
			$w = 300;
			$h = 100;
			$new_image = imagecreatetruecolor($w, $h);
			$bg = imagecolorallocate($new_image, 255, 255, 255);
			imagefill($new_image, 0, 0, $bg);
			imagecopyresampled($new_image, $old_image, 0, 0, 0, 0, $w, $h, $bg_image_info[0], $bg_image_info[1]);
			$bg = imagecolorallocate($new_image, 255, 255, 255);
			$codeLength = strlen($code);
			for ($i = 0; $i < $codeLength; ++$i) {
				$color_cols = explode(',', '0,0,0');
				$fg = imagecolorallocate($new_image, trim($color_cols[0]), trim($color_cols[1]), trim($color_cols[2]));
				imagettftext($new_image, 30, mt_rand(-10, 15), 10 + ($i * 36), mt_rand($h - 10, $h - 5), $fg, public_path('/luxisr.ttf'), $code[$i]);
			}
			imagealphablending($new_image, false);

			imagejpeg($new_image, $file, 90);

			imagedestroy($new_image);

		});
	}


	public function getFilePath($skipPublic = false) {
		$key = $this->key;
		$filepath = public_path('uploads');
		if (!is_dir($filepath)) {
			mkdir($filepath, 0777, true);
		}
		$filepath .= '/'.$key;
		$filepath .= '.jpg';
		if ($skipPublic) {
			return strtr($filepath, [public_path()=>'']);
		}
		return $filepath;
	}

	public function getBgPath() {
		return public_path('/captcha.jpg');
	}

	public static function getFreeCaptcha() {
		$x = self::where('active', false)->orderBy(\DB::raw('RANDOM()'))->first();
		if (empty($x)) {
			return self::create();
		} else {
			return $x;
		}
	}

	public static function validate($data) {
		$key = @$data['captcha']['key'];
		$code = @$data['captcha']['code'];
		if (!empty($key) && !empty($code)) {
			$key = explode('i', $key);
			$id = @$key[0];
			$key = @$key[1];
			if (!empty($id) && !empty($key)) {
				$captcha = self::find((int) $id);
				if ($captcha) {
					if ($captcha->key == $key) {
						if ($captcha->code == strtoupper($code) && !$captcha->active) {
							$captcha->active = true;
							$captcha->save();
							return true;
						}
					}
				}
				return false;
			} else {
				return false;
			}
		}
		return false;
	}
}