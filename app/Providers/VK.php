<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 24.10.15
 * Time: 16:54
 */

namespace App\Providers;


class VK {

	public static function getGet($id)
	{
		$p = VkProvider::getInstance();
		try {
			$data = @$p->api('users.get', ['user_ids' => $id, 'fields'=>'photo_100,first_name,last_name']);
			if ($data) {
				if ( !empty($data['response'])) {
					$user = array_shift($data['response']);
					return [
						'id' => @$user['id'],
						'first_name'=> @$user['first_name'],
						'last_name' => @$user['last_name'],
						'avatar' => @$user['photo_100']
					];
				}
			}
		} catch (\Exception $e) {
			return false;
		}
		return false;
	}

	public static function resolveScreenNames($names)
	{
		if (is_array($names)) {

			try {
				$p = VkProvider::getInstance();
				$data = @$p->api('users.get', ['user_ids' => implode(',',$names), 'fields'=>'photo_100,first_name,last_name']);
				if ($data) {
					if ( !empty($data['response'])) {
						return array_map(  function($item) { return (int)$item['id']; }, $data['response'] );
					}
				}
			} catch (\Exception $e) {
				return [];
			}

		} else {
			return [];
		}
	}

}