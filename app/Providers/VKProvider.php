<?php
/**
 * Created by PhpStorm.
 * User: iVan
 * Date: 22.07.2015
 * Time: 23:38
 */

namespace App\Providers;


use App\VkToken;

class VkProvider {

	private static $self;


	public static function getInstance($clearCache = false)
	{
		if (empty(self::$self) || $clearCache) {
			self::$self = new VkProvider($clearCache);
		}
		return self::$self;
	}

	public function api($method, $params) {
		$url = 'https://api.vk.com/method/'.$method."?lang=ru&v=5.34";
		$p = [];
		foreach($params as $k=>$v) {
			$p[] = $k.'='.$v;
		}
		$p = implode('&',$p);
		$url = $url.'&'.$p;
		$opts = array('http' =>
			array(
				'method'  => 'GET',
				'timeout' => 5,
				'ignore_errors' => true,
			)
		);
		$context  = stream_context_create($opts);
		@$data = file_get_contents($url, NULL, $context);
		$data = json_decode($data,true);
		return $data;
	}

	public function getAppPoll() {
		return [
			'4166631' => [
				'id' => '4166631',
				'secret' => 'd9mmoOTWdMbI9k97zWk3'
			],
			'4064842' => [
				'id' => '4064842',
				'secret' => 'vtzuxR37MbQ39VudylOM',
			],
			'3694551' => [
				'id' => '3694551',
				'secret' => 'GREKqfZVTyGUIHPCBvmN'
			]
		];
	}

	public function getLinks() {

		$arLinks = [];

		foreach($this->getAppPoll() as $app) {
			$arLinks[] = $this->getAuthUrl($app);
		}
		return $arLinks;
	}


	public function getRandomToken() {
		$token = VkToken::orderBy(\DB::raw('RAND()'))->first();
		if ($token) {
			return $token;
		} else {
			return false;
		}
	}

	public function putToken($data) {
		return VkToken::create($data);
	}

	private function getAuthUrl($app)
	{
		$url = 'https://oauth.vk.com/authorize';
		$url .= "?client_id=".$app['id'];
		$url .= '&redirect_uri='.$this->getAuthEndpoint();
		$url .= "&display=page";
		$url .= '&response_type=code';
	}
}
