
<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 30.10.15
 * Time: 23:12
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class VkToken extends Model {

	protected $table = 'vk_token';
	protected $fillable = ['app','user_id','token'];

	public function __toString() {
		return (string)$this->token;
	}
}