<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 24.10.15
 * Time: 1:19
 */

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Request;

class Post extends Model {

	use SoftDeletes;
	protected $table = 'post';

	protected $fillable = [
		'ip','user_id','key','star','name','text'
	];

	protected $hidden = ['ip','updated_at','key'];

	protected $dates = [
		'created_at','updated_at','deleted_at'
	];


	public function deleteByKey($key) {
		if ($this->key == $key) {
			$this->delete();
			return true;
		} else {
			return false;
		}
	}

	public static function boot() {
		parent::boot();

		self::creating( function ($model) {
			$model->key = uniqid('t',true);
			if (App::runningInConsole())
			{
				$model->ip = 'local';
			} else {
				$model->ip = Request::getClientIp();
			}

			$u = User::find($model->user_id);
			if (empty($u)) {
				$u = User::create([
					'sync' => false,
					'first_name' => '',
					'last_name' => '',
					'avatar' => '',
					'star_count' => 0,
					'star_sum' => 0,
					'id' => $model->user_id
				]);
			}
			$u->star_count ++;
			$u->star_sum += $model->star;
			$u->save();
			return true;
		} );
	}

	public function toArray($showKey = false) {
		$data = parent::toArray();
		if ($showKey) {
			$data['key'] = $this->key;
		}
		return $data;
	}
}