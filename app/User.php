<?php
/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 24.10.15
 * Time: 1:19
 */

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Request;

class User extends Model {


	protected $table = 'users';

	protected $fillable = [
		'first_name','last_name','avatar','star_sum','star_count','rating','sync','id'
	];

	protected $dates = [
		'created_at','updated_at','deleted_at'
	];

	protected $hidden = [
		'sync','created_at','updated_at','deleted_at'
	];

	protected $casts = [
		'star_sum'=>"integer",
		'star_count'=>'integer',
		'rating' => 'float',
		'sync' => 'boolean'
	];

	public function toArray() {
		$data = parent::toArray();

		if (!isset($data['star_sum'])) { $data['star_sum'] = 5; }
		if (!isset($data['star_count'])) { $data['star_count'] = 1; }
		if (!isset($data['rating'])) { $data['rating'] = 5; }
		if (!isset($data['last_name'])) { $data['last_name'] = ''; }
		if (!isset($data['first_name'])) { $data['first_name'] = ''; }
		if (empty($data['avatar'])) { $data['avatar'] = 'http://vk.com/images/deactivated_100.png'; }
		return $data;
	}


	public static function boot() {
		parent::boot();

		self::saving(function ($model) {
			$model->rating = $model->calcRating();
		});

		self::saved( function ($model) {
			$now = new \DateTime();
			if (!$model->sync && $model->updated_at->modify('+1second') >= $now) {
				$id = $model->id;
				\Queue::push(function($job) use ($id) {
					$job->delete();
					$data = App\Providers\VK::getGet($id);
					if ($data) {
						$user = User::find((int) @$data['id']);
						if ($user) {
							unset($data['id']);
							$user->fill($data);
							$user->sync = true;
							$user->save();
						}
					}
				});
			}
		} );
	}

	public function calcRating() {
		if ($this->star_count > 0) {
			return $this->star_sum / $this->star_count;
		} else {
			return 5;
		}
	}

}