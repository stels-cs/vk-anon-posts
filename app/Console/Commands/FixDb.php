<?php

namespace App\Console\Commands;

use App\User;
use DB;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class FixDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixdb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'recalc user rating';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arUsers = User::all();
		foreach($arUsers as $user) {
			$starSum = DB::select('SELECT sum(star) s from post where user_id = '.$user->id);
			$strCount = DB::select('SELECT count(*) c from post where user_id = '.$user->id);
			$starSum = (int)@$starSum[0]->s;
			$strCount = (int)@$strCount[0]->c;
			$this->info(''.$user->id.' '.$starSum.' '.$strCount);
			$user->star_sum = $starSum;
			$user->star_count = $strCount;
			$user->save();
		}
    }
}
