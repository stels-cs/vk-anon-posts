<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class Captcha extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'captcha';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate captcha';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $freesCaptchas = \App\Captcha::where('active', false)->count();
		$this->comment('Captchas count: '.$freesCaptchas);
		if ($freesCaptchas < 1000) {
			$this->comment('Start generationg');
			$start = microtime(true);
			for ($i = 0; $i<10000; $i++) {
				\App\Captcha::create();
			}
			$end = microtime(true);
			$this->comment('End after '.($end-$start).' s');
		}
    }
}
