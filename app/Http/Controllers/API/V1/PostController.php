<?php
namespace App\Http\Controllers\API\V1;
use App\Captcha;
use App\Http\Controllers\Controller;
use App\Post;
use App\Providers\VK;
use App\User;
use Illuminate\Http\Request;


/**
 * Created by PhpStorm.
 * User: Ivan
 * Date: 24.10.15
 * Time: 0:08
 */

class PostController extends Controller {

	const MAX_POST = 20;
	const MAX_OFFSET = 200;
	const SPAM_COUNT = 5;
	const USER_DDOS = 60;
	const LIMIT_FOR_IP = 100;

	public function index(Request $r) {
		if ($r->has('user_ids') || $r->has('user_screen_names')) {
			return $this->getList($r);
		}
		$userId = $r->get('user_id');
		$screenName = $r->get('screen_name');
		$offset = (int) $r->get('offset',0);
		$offset = max($offset, 0);
		$offset = min($offset, self::MAX_OFFSET);
		if (!empty($userId) || !empty($screenName)) {
			if (!empty($screenName)) {
				if ( false == ($userId = $this->parseScreenName($screenName)) ) {
					return response()->json([
						'errors' => [
							'screen_name' => trans('main.cant-parse-screen-name')
						]
					], 422);
				}
			}
			if (!empty($userId)) {
				$arPosts = $this->getPostFor($userId, $offset);
				return response()->json([
					'response' => [
						'user' => User::find($userId),
						'user_id' => $userId,
						'posts' => $arPosts,
						'more' => ( count($arPosts)>self::MAX_POST && $offset < self::MAX_OFFSET - self::MAX_POST)
					]
				]);
			} else {
				return response()->json([
					'errors' => [
						'user_id' => trans('main.no-target')
					],
					'debug-data' => [
						'state'=>2
					]
				], 422);
			}
		} else {
			return response()->json([
				'errors' => [
					'user_id' => trans('main.no-target')
				],
				'debug-data' => [
					'state'=>1
				]
			],422);
		}
	}

	public function store(Request $r) {

		$validate  = \Validator::make( $r->all(), [
			'text' => 'string|min:1|max:500',
			'user_id' => 'numeric',
			'screen_name' => 'string',
			'star' => 'digits_between:1,5',
			'name' => 'string|max:50'
		]) ;

		if ($validate->invalid()) {
			return response()->json($validate->errors(), 422);
		}

		if ( !$this->validateIp() && !Captcha::validate( $r->all() ) ) {
			return response()->json([
				'errors' => [
					'ip' => trans('main.ip-block'),
				],
				'captcha' => Captcha::getFreeCaptcha()
			],400);
		}

		if (!$this->validateIpNet() && !Captcha::validate( $r->all() ) ) {
			return response()->json([
				'errors' => [
					'ip' => trans('main.ip-net-block'),
				],
				'captcha' => Captcha::getFreeCaptcha()
			],400);
		}

		$text = $r->get('text');
		$errorText = trans('main.unknown-error');
		if ($this->validateText($text, $errorText)) {

			if ($this->validateRequest($r, $errorText, $data)) {

				$data['text'] = $text;
				$userDdosKey = $this->getUserDDOSKey($data);
				if ($this->validateUserDDos($userDdosKey)) {
					$key = $this->antiSpamKey($data);
					if ($this->validateSpam($key, $count) && !Captcha::validate( $r->all() ) ) {
						$post = Post::create($data);
						\Cache::put($key, $count + 1, 1);

						return response()->json([
							"response" => ['post' => $post->toArray(true)]
						]);
					} else {
						return response()->json([
							'errors' => [
								'spam' => trans('main.spam'),
							],
							'captcha' => Captcha::getFreeCaptcha()
						], 400);
					}
				} else {
					return response()->json([
						'errors' => [
							'spam' => trans('main.user-ddos')
						]
					], 400);
				}

			} else {
				return response()->json([
					'errors' => [
						'request' => $errorText
					]
				], 500);
			}

		} else {
			return response()->json([
					'errors' => [
						'text' => $errorText
					]
				], 400);
		}
	}

	private function validateText(&$text, &$errorText)
	{
		$text = trim($text);
		if (preg_match('/([A-z]+\.[A-z]{2,5})/usi', $text)) {
			$errorText = trans('main.has-link');
			return false;
		}

		$text = preg_replace('/([^A-zЪХЗЩШГНЕКУЦЙФЫВАПРОЛДЖЭЁЮБЬТИМСЧЯА-яЁё0-9\!\ \.\,\@\$\-\+\=\)\(\*\%\"\'\:\?\n])/usi', '', $text);
		$text = preg_replace('/(\W)[\W]{4,9999}/usi', '$1', $text);
		if (mb_strlen($text) < 3) {
			$errorText = trans('main.short-text');
			return false;
		}
		if (mb_strlen($text) > 500) {
			$errorText = trans('main.long-text');
			return false;
		}
		if (mb_strpos($text, 'http') !== false) {
			$errorText = trans('main.has-link');
			return false;
		}
		return true;
	}

	private function validateRequest($r, &$errorText, &$data)
	{
		$data = [];
		$uId = $r->get('user_id');
		$screenName = $r->get('screen_name');
		if (empty($uId) && empty($screenName)) {
			$errorText = trans('main.no-target');
			return false;
		}

		if (empty($uId) && !empty($screenName)) {
			if ($uId = $this->parseScreenName($screenName)) {
				$data['user_id'] = (int) $uId;
			} else {
				$errorText = trans('main.cant-parse-screen-name');
				return false;
			}
		} else {
			$data['user_id'] = (int) $uId;
		}

		if (empty($data['user_id'])) {
			$errorText = trans('main.no-target');
			return false;
		}

		$data['star'] = $r->get('star', 5);
		$data['star'] =  min(5, max((int)$data['star'], 1) );  // star [1..5]
		$data['name'] = $r->get('name', '');
		if ($data['name']) {
			$data['name'] = preg_replace('/[^А-яЁёA-z0-9\ ]/usi', '', $data['name']);
			$data['name'] = trim($data['name']);
			$data['name'] = mb_substr($data['name'], 0, 50);
			$data['name'] = preg_replace('/[\W]{2,999}/us', ' ', $data['name']);

		}
		return true;
 	}

	private function parseScreenName($screenName)
	{
		$data = VK::getGet($screenName);
		if ($data && @$data['id']) {
			return (int)$data['id'];
		}
		return false;
	}

	public function destroy(Request $r, $id) {

		$validate  = \Validator::make( $r->all(), [
			'key' => 'required|string'
		]) ;

		if ($validate->invalid()) {
			return response()->json($validate->errors(), 422);
		}

		$key = $r->get('key');

		$post = Post::find($id);
		if ($post) {
			if ($post->deleteByKey($key)) {
				return response()->json(['response'=>[ "success"=>"ok", 'message'=>trans('main.successful-deleted') ]]);
			}
		}
		return response()->json([
			'errors'=>[
				'key' => trans('main.key-invalid-or-expired'),
			]
		], 422);
	}

	private function getPostFor($userId, $offset)
	{
		$arPosts = Post::where('user_id', (int) $userId)->orderBy('id','DESC')->take(self::MAX_POST+1)->skip((int)$offset)->get()->values();
		return $arPosts;
	}

	private function antiSpamKey($data)
	{
		$ip = $this->getIp();
		$key = $ip.md5($data['text']);
		return $key;
 	}

	private function validateSpam($key, &$count)
	{
		$count = \Cache::get($key);
		$count = (int) $count;
		if ((int)$count > self::SPAM_COUNT) {
			return false;
		} else {
			return true;
		}
	}

	private function getUserDDOSKey($data)
	{
		return 'up'.$data['user_id'];
	}

	private function validateUserDDos($userDdosKey)
	{
		$count = \Cache::get($userDdosKey, 0);
		$count = (int) $count;
		if ($count > self::USER_DDOS) {
			return false;
		} else {
			$count++;
			\Cache::put($userDdosKey, $count, 1);
			return true;
		}
	}

	private function getIp()
	{
		$ip = 'local';
		if (!\App::runningInConsole()) {
			$ip = \Request::getClientIp();
		}
		return $ip;
	}

	private function validateIp()
	{
		$ip = $this->getIp();
		$key = 'ip'.$ip;
		$count = \Cache::get($key, 0);
		$count = (int) $count;
		$count++;
		\Cache::put($key, $count, 10);
		return $count < self::LIMIT_FOR_IP;
	}

	private function validateIpNet()
	{
		return true;
	}

	private function getList($r)
	{
		$ids = $r->get('user_ids', []);
		$names = $r->get('user_screen_names',[]);


		if (!empty($ids)) {
			if (!is_array($ids)) {
				$ids = explode(',',$ids);
			}
			$ids = array_map( function ($item) { return (int) $item; }, $ids );
			$ids = array_filter($ids);
		}

		if (!empty($names)) {
			if (!is_array($names)) {
				$names = explode(',',$names);
			}
			$names = array_map( function ($item) { return trim($item); }, $names );
			$names = array_filter($names);
		}

		if (!empty($names)) {
			$idsn = $this->explodeNames($names);
			$ids = array_merge($idsn, $ids);
		}

		if (!empty($ids)) {
			$arUser = User::whereIn('id',$ids)->get();
			if ($arUser->count() != count($ids)) {
				$notExist = array_diff($ids, $arUser->map( function ($x) { return $x->id; } )->all());
				foreach ($notExist as $id) {
					$arUser->push( User::create(['id'=>$id]) );
				}
			}
			return response()->json( ['response' => $arUser->all()]);
		}

		return response()->json([
			'errors' => [
				'user_id' => trans('main.no-target')
			],
			'debug-data' => [
				'state'=>2
			]
		], 422);
	}

	private function explodeNames($names)
	{
		$ids = VK::resolveScreenNames($names);
		return $ids;
	}

}