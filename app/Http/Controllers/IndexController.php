<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    public function getIndex() {

		return view('main.index');
	}

	public function getStat() {
		return view('stat.index');
	}

	public function getTotal() {
		$data = [];
		$postCount = \App\Post::all()->count();
		$data = ['posts'=>$postCount];
		return response()->json($data);
	}

	public function getLast() {
		$arPost = \App\Post::orderBy('created_at', 'DESC')->take(3*10)->get();
		return response()->json($arPost);
	}
}
